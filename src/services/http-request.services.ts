import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { createHttpParams } from '../lib/http-params';

interface Params {
  page?: number;
  with_genres?: string;
}

@Injectable({
  providedIn: 'root',
})
export class HttpRequestServices {
  public key: string = environment.moviesApi.key;
  baseUrl: string = environment.moviesApi.route;

  constructor(private http: HttpClient) {}

  get(path: string, options?: Params): Observable<any> {
    const params: HttpParams = createHttpParams({ api_key: this.key, ...options });

    return this.http.get(`${this.baseUrl}/${path}`, { params });
  }
}

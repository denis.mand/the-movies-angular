export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyCpRaGsuOw1k30f1W7CFOsD_y4-gyCQyWs',
    authDomain: 'the-movies-angular.firebaseapp.com',
    databaseURL: 'https://the-movies-angular.firebaseio.com',
    projectId: 'the-movies-angular',
    storageBucket: 'the-movies-angular.appspot.com',
    messagingSenderId: '1017982307284',
    appId: '1:1017982307284:web:59e5c2bc06a8b58d15a441',
  },
  moviesApi: {
    key: 'e2e6c0d0272e7a10dfb1d89d1580078e',
    route: 'https://api.themoviedb.org/3',
  },
};

import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appToggleNextElement]',
  exportAs: 'appToggleNextElement',
})
export class ToggleNextElementDirective {
  constructor(private renderer: Renderer2, private el: ElementRef) {}

  @HostListener('click')
  toggleClass() {
    const hasClass = this.el.nativeElement.classList.contains('collapsed');

    if (hasClass) {
      this.el.nativeElement.classList.remove('collapsed');
      this.renderer.addClass(this.el.nativeElement.nextElementSibling, 'show');
    } else {
      this.el.nativeElement.classList.add('collapsed');
      this.renderer.removeClass(this.el.nativeElement.nextElementSibling, 'show');
    }
  }
}

import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appCollapseNavMenu]',
  exportAs: 'appCollapseNavMenu',
})
export class CollapseNavMenuDirective {
  constructor(private renderer: Renderer2, private el: ElementRef) {}

  @HostListener('click')
  collapseNavMenu() {
    const isDropdown = this.el.nativeElement.parentElement.classList.contains('dropdown-menu');

    if (isDropdown) {
      const parent = this.el.nativeElement.parentElement;
      const menu = parent.parentElement.parentElement;
      this.renderer.removeClass(parent, 'show');
      this.renderer.removeClass(menu, 'show');
      this.renderer.addClass(menu.previousElementSibling, 'collapsed');
    } else {
      const menu = this.el.nativeElement.parentElement.parentElement;
      this.renderer.removeClass(menu, 'show');
      this.renderer.addClass(menu.previousElementSibling, 'collapsed');
    }
  }
}

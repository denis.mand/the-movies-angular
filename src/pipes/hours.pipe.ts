import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hours',
})
export class HoursPipe implements PipeTransform {
  transform(value: number): string {
    const hours: number = Math.floor(value / 60);
    return (
      hours.toString().padStart(2, '0') +
      ':' +
      (value - hours * 60).toString().padStart(2, '0') +
      'h'
    );
  }
}

import { Component, Input } from '@angular/core';
import { ICast } from '../interface/cast.interface';

@Component({
  selector: 'app-cast',
  templateUrl: 'cast.component.html',
  styleUrls: ['cast.component.scss'],
})
export class CastComponent {
  private _cast: ICast[];

  @Input()
  set cast(cast: ICast[]) {
    this._cast = cast;
  }
  get cast() {
    return this._cast;
  }
}

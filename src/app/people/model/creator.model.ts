import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CreatorModel {
  public id: number;
  public department: string;
  public original_language: string;
  public original_title: string;
  public job: string;
  public overview: string;
  public vote_count: number;
  public video: boolean;
  public poster_path: string | null;
  public backdrop_path: string | null;
  public title: string;
  public popularity: number;
  public genre_ids: number[];
  public vote_average: number;
  public adult: boolean;
  public release_date: string;
  public credit_id: string;
}

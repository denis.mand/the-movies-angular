import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ActorModel {
  public character: string;
  public credit_id: string;
  public release_date: string;
  public vote_count: number;
  public video: boolean;
  public adult: boolean;
  public vote_average: number;
  public title: string;
  public genre_ids: number[];
  public original_language: string;
  public original_title: string;
  public popularity: number;
  public id: number;
  public backdrop_path: string | null;
  public poster_path: string | null;
  public overview: string;
}

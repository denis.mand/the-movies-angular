import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestServices } from '../../../services/http-request.services';
import { IPerson } from '../interface/person.interface';

@Injectable()
export class PeopleDataSource {
  constructor(private http: HttpRequestServices) {}

  getCastAndCrew(id: number): Observable<IPerson> {
    return this.http.get(`person/${id}/movie_credits`);
  }
}

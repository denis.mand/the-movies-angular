import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { PeopleDataSource } from './people.datasource';
import { IPerson } from '../interface/person.interface';

@Injectable()
export class PeopleRepository {
  imageRoute: string = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2';

  constructor(private dataSource: PeopleDataSource) {}

  getCastAndCrew(id): Observable<IPerson> {
    return this.dataSource.getCastAndCrew(id).pipe(
      map(item => {
        item.cast.map(actor => (actor.poster_path = this.imageRoute + actor.poster_path));
        item.crew.map(creator => (creator.poster_path = this.imageRoute + creator.poster_path));
        return item;
      })
    );
  }
}

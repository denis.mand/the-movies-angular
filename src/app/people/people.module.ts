import { NgModule } from '@angular/core';
import { ActorModel } from './model/actor.model';
import { CreatorModel } from './model/creator.model';
import { PeopleDataSource } from './model/people.datasource';
import { PeopleRepository } from './model/people.repository';
import { CastComponent } from './cast/cast.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [BrowserModule],
  providers: [ActorModel, CreatorModel, PeopleDataSource, PeopleRepository],
  declarations: [CastComponent],
  exports: [CastComponent],
})
export class PeopleModule {}

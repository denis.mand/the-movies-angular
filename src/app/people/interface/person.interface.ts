import { ActorModel } from '../model/actor.model';
import { CreatorModel } from '../model/creator.model';

export interface IPerson {
  cast: ActorModel[];
  crew: CreatorModel[];
  id: number;
}

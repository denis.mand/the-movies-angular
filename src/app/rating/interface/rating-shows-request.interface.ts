import { RatingShowModel } from '../model/rating-show.model';
import { IPaginationRequest } from '../../interface/pagination-request.interface';

export interface IRatingShowsRequest extends IPaginationRequest {
  results: RatingShowModel[];
}

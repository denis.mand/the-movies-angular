import { RatingMoviesModel } from '../model/rating-movies.model';
import { IPaginationRequest } from '../../interface/pagination-request.interface';

export interface IRatingMoviesRequest extends IPaginationRequest {
  results: RatingMoviesModel[];
}

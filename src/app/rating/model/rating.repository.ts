import { Injectable } from '@angular/core';
import { RatingDataSource } from './rating.datasource';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IRatingMoviesRequest } from '../interface/rating-movies-request.interface';
import { IRatingShowsRequest } from '../interface/rating-shows-request.interface';

@Injectable()
export class RatingRepository {
  imageRoute: string = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2';
  constructor(private dataSource: RatingDataSource) {}

  getMoviesRating(page?: number): Observable<IRatingMoviesRequest> {
    return this.dataSource.getMoviesRating(page).pipe(
      map(item => {
        item.results.map(movie => {
          if (movie.poster_path) movie.poster_path = this.imageRoute + movie.poster_path;
        });
        return item;
      })
    );
  }

  getShowsRating(page?: number): Observable<IRatingShowsRequest> {
    return this.dataSource.getShowsRating(page).pipe(
      map(item => {
        item.results.map(show => {
          if (show.poster_path) show.poster_path = this.imageRoute + show.poster_path;
        });
        return item;
      })
    );
  }
}

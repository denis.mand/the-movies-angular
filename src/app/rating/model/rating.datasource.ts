import { Injectable } from '@angular/core';
import { HttpRequestServices } from '../../../services/http-request.services';
import { Observable } from 'rxjs';
import { IRatingMoviesRequest } from '../interface/rating-movies-request.interface';
import { IRatingShowsRequest } from '../interface/rating-shows-request.interface';

@Injectable()
export class RatingDataSource {
  constructor(private http: HttpRequestServices) {}

  getMoviesRating(page?: number): Observable<IRatingMoviesRequest> {
    return this.http.get('movie/top_rated', { page: page });
  }

  getShowsRating(page?: number): Observable<IRatingShowsRequest> {
    return this.http.get('tv/top_rated', { page: page });
  }
}

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class RatingShowModel {
  public poster_path: string | null;
  public first_air_date: string;
  public overview: string;
  public genre_ids: number[];
  public id: number;
  public original_country: string[];
  public original_name: string;
  public original_language: string;
  public name: string;
  public backdrop_path: string | null;
  public popularity: number;
  public vote_count: number;
  public vote_average: number;
}

import { NgModule } from '@angular/core';
import { MoviesModule } from '../movies/movies.module';
import { RatingComponent } from './rating.component';
import { RatingRepository } from './model/rating.repository';
import { RatingDataSource } from './model/rating.datasource';
import { HttpRequestServices } from '../../services/http-request.services';
import { NgxPaginationModule } from 'ngx-pagination';
import { BrowserModule } from '@angular/platform-browser';
import { RatingRouterGuard } from './rating-router.guard';
import { TvShowsModule } from '../tv-shows/tv-shows.module';

@NgModule({
  imports: [MoviesModule, NgxPaginationModule, BrowserModule, TvShowsModule],
  providers: [RatingRepository, RatingDataSource, HttpRequestServices, RatingRouterGuard],
  declarations: [RatingComponent],
  exports: [RatingComponent],
})
export class RatingModule {}

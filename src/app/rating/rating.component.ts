import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { RatingRepository } from './model/rating.repository';
import { RatingMoviesModel } from './model/rating-movies.model';
import { ActivatedRoute } from '@angular/router';
import { RatingShowModel } from './model/rating-show.model';

@Component({
  templateUrl: 'rating.component.html',
  styleUrls: ['rating.component.scss'],
})
export class RatingComponent {
  rating: RatingMoviesModel[] | RatingShowModel[];
  totalResults: number;
  page: number;
  type: string;

  constructor(private repository: RatingRepository, private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.type = params['type'];
      this.loadData();
    });
  }

  loadData(page: number = 1) {
    this.type === 'movies' && this.getMoviesRating(page);
    this.type === 'tv-shows' && this.getShowsRating(page);
  }

  getMoviesRating(page: number = 1): void {
    this.repository.getMoviesRating(page).subscribe(item => {
      this.rating = item.results;
      this.totalResults = item.total_results;
    });
  }

  getShowsRating(page: number = 1): void {
    this.repository.getShowsRating(page).subscribe(item => {
      this.rating = item.results;
      this.totalResults = item.total_results;
    });
  }

  pageChange(page: number) {
    this.page = page;
    this.loadData(page);
  }
}

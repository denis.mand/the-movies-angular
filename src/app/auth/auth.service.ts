import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  userData: Observable<firebase.User>;

  constructor(private angularFireAuth: AngularFireAuth) {
    this.userData = angularFireAuth.authState;
  }

  async signUp(email: string, password: string) {
    const response = await this.angularFireAuth.auth.createUserWithEmailAndPassword(
      email,
      password
    );

    localStorage.setItem('token', response.user.uid);
  }

  async signIn(email: string, password: string) {
    const response = await this.angularFireAuth.auth.signInWithEmailAndPassword(email, password);

    localStorage.setItem('token', response.user.uid);
  }

  async signOut() {
    await this.angularFireAuth.auth.signOut();
    localStorage.removeItem('token');
  }

  userIsLogged(): boolean {
    return !!localStorage.getItem('token');
  }
}

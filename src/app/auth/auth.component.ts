import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from './auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: 'auth.component.html',
  styleUrls: ['auth.component.scss'],
})
export class AuthComponent {
  public email: string;
  public password: string;
  public hasAccount: boolean = true;
  public errorMessage: string;

  constructor(
    private router: Router,
    private auth: AuthenticationService,
    private toastr: ToastrService
  ) {}

  authenticate(form: NgForm): void {
    if (!form.valid) {
      this.errorMessage = 'Form Data Invalid';
      this.toastr.error(this.errorMessage);
      return;
    }
    if (this.hasAccount) {
      this.login();
    } else {
      this.registration();
    }
    this.email = '';
    this.password = '';
  }

  private login(): void {
    this.auth
      .signIn(this.email, this.password)
      .then(() => {
        this.router.navigateByUrl('/');
      })
      .catch(error => {
        this.toastr.error(error.message);
      });
  }

  private registration(): void {
    this.auth
      .signUp(this.email, this.password)
      .then(() => {
        this.router.navigateByUrl('/');
      })
      .catch(error => {
        this.toastr.error(error.message);
      });
  }

  setHasAccount(status): void {
    this.hasAccount = status;
  }
}

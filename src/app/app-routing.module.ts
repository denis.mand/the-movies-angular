import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules, Router } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { MoviesComponent } from './movies/movies.component';
import { MovieComponent } from './movies/movie/movie.component';
import { RatingComponent } from './rating/rating.component';
import { RatingRouterGuard } from './rating/rating-router.guard';
import { TvShowsComponent } from './tv-shows/tv-shows.component';
import { TvShowComponent } from './tv-shows/tv-show/tv-show.component';
import { UpcomingComponent } from './movies/upcoming/upcoming.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'login',
    component: AuthComponent,
  },
  {
    path: 'movies/:id',
    component: MoviesComponent,
  },
  {
    path: 'movies',
    component: MoviesComponent,
  },
  {
    path: 'movie/:id',
    component: MovieComponent,
  },
  {
    path: 'tv-shows/:id',
    component: TvShowsComponent,
  },
  {
    path: 'tv-shows',
    component: TvShowsComponent,
  },
  {
    path: 'tv-show/:id',
    component: TvShowComponent,
  },
  {
    path: 'upcoming',
    component: UpcomingComponent,
  },
  {
    path: 'rating/:type',
    component: RatingComponent,
    canActivate: [RatingRouterGuard],
  },
  { path: '**', redirectTo: '/' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  providers: [RatingRouterGuard],
  exports: [RouterModule],
})
export class AppRoutingModule {}

import { Injectable } from '@angular/core';
import { ICreator } from '../tv-show/interface/creator.interface';
import { IGenre } from '../../movies/interfaces/genres.interface';
import { ILastEpisodeToAir } from '../tv-show/interface/last-episode-to-air.interface';
import { IProductionCompanies } from '../../movies/interfaces/production-companies.interface';
import { ISeason } from '../tv-show/interface/season.interface';

@Injectable({
  providedIn: 'root',
})
export class TvShowModel {
  public backdrop_path: string | null;
  public first_air_date: string;
  public homepage: string;
  public id: number;
  public name: string;
  public origin_country: string[];
  public original_language: string;
  public original_name: string;
  public overview: string;
  public popularity: number;
  public poster_path: string | null;
  public vote_average: number;
  public vote_count: number;
  public created_by?: ICreator[];
  public episode_run_time?: number[];
  public genres?: IGenre[];
  public in_production?: boolean;
  public languages?: string[];
  public last_air_date?: string;
  public last_episode_to_air?: ILastEpisodeToAir[];
  public next_episode_to_air?: string | null;
  public networks?: string[];
  public number_of_episodes?: number;
  public number_of_seasons?: number;
  public production_companies?: IProductionCompanies[];
  public seasons?: ISeason[];
  public status?: string;
  public type?: string;
  public genre_ids?: number[];
}

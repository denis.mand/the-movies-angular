import { Injectable } from '@angular/core';
import { HttpRequestServices } from '../../../services/http-request.services';
import { Observable } from 'rxjs';
import { ITvShowsRequest } from '../interface/tv-shows-request.interface';
import { TvShowModel } from './tv-show.model';

@Injectable()
export class TvShowDatasource {
  constructor(private http: HttpRequestServices) {}

  getTvShows(page?: number, genre?: string): Observable<ITvShowsRequest> {
    return this.http.get('discover/tv', { page, with_genres: genre });
  }
  getTvShow(id: number): Observable<TvShowModel> {
    return this.http.get(`tv/${id}`);
  }
}

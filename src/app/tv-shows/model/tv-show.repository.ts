import { Injectable } from '@angular/core';
import { TvShowDatasource } from './tv-show.datasource';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ITvShowsRequest } from '../interface/tv-shows-request.interface';
import { ITvShowRequest } from '../tv-show/interface/tv-show-request.interface';
import { TvShowModel } from './tv-show.model';

@Injectable()
export class TvShowRepository {
  imageRoute: string = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2';

  constructor(private dataSource: TvShowDatasource) {}

  getTvShows(page?: number, genre?: string): Observable<ITvShowsRequest> {
    return this.dataSource.getTvShows(page, genre).pipe(
      map(item => {
        item.results.map(movie => {
          if (movie.poster_path) {
            movie.poster_path = this.imageRoute + movie.poster_path;
          }
        });
        return item;
      })
    );
  }
  getTvShow(id: number): Observable<TvShowModel> {
    return this.dataSource.getTvShow(id).pipe(
      map(show => {
        if (show.poster_path) {
          show.poster_path = this.imageRoute + show.poster_path;
        }
        show.seasons.map(season => {
          season.poster_path = season.poster_path
            ? this.imageRoute + season.poster_path
            : season.poster_path;
        });
        return show;
      })
    );
  }
}

import { Component, Input } from '@angular/core';
import { TvShowModel } from '../model/tv-show.model';

@Component({
  selector: 'serial-card',
  templateUrl: 'tv-show-card.component.html',
})
export class TvShowCardComponent {
  private _show: TvShowModel;

  @Input()
  set show(show: TvShowModel) {
    this._show = show;
  }

  get show(): TvShowModel {
    return this._show;
  }
}

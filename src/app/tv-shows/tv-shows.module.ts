import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from '../app-routing.module';
import { LayoutModule } from '../layout/layout.module';
import { PeopleModule } from '../people/people.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { TvShowComponent } from './tv-show/tv-show.component';
import { TvShowsComponent } from './tv-shows.component';
import { MoviesModule } from '../movies/movies.module';
import { TvShowRepository } from './model/tv-show.repository';
import { TvShowDatasource } from './model/tv-show.datasource';
import { TvShowCardComponent } from './card/tv-show-card.component';
import { SWIPER_CONFIG, SwiperConfigInterface, SwiperModule } from 'ngx-swiper-wrapper';
import { SeasonsSliderComponent } from './seasons/seasons-slider.component';
import { HoursPipe } from '../../pipes/hours.pipe';

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  slidesPerView: 1,
  direction: 'horizontal',
  slideToClickedSlide: true,
  a11y: true,
  mousewheel: true,
  scrollbar: false,
  watchSlidesProgress: true,
  navigation: true,
  keyboard: true,
  pagination: false,
  centeredSlides: true,
  autoHeight: true,
  loop: false,
};

@NgModule({
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    LayoutModule,
    PeopleModule,
    NgxPaginationModule,
    MoviesModule,
    SwiperModule,
  ],
  providers: [
    TvShowRepository,
    TvShowDatasource,
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG,
    },
  ],
  declarations: [TvShowComponent, TvShowsComponent, TvShowCardComponent, SeasonsSliderComponent],
  exports: [TvShowComponent, TvShowsComponent, TvShowCardComponent, SeasonsSliderComponent],
})
export class TvShowsModule {}

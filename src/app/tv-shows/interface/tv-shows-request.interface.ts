import { IPaginationRequest } from '../../interface/pagination-request.interface';
import { TvShowModel } from '../model/tv-show.model';

export interface ITvShowsRequest extends IPaginationRequest {
  results: TvShowModel[];
}

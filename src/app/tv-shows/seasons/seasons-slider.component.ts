import { Component, Input } from '@angular/core';
import { ISeason } from '../tv-show/interface/season.interface';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';

@Component({
  selector: 'serial-seasons-slider',
  templateUrl: './seasons-slider.component.html',
  styleUrls: ['./season.component.scss'],
})
export class SeasonsSliderComponent {
  _seasons: ISeason[];

  @Input()
  set seasons(seasons: ISeason[]) {
    this._seasons = seasons;
  }
  get seasons() {
    return this._seasons;
  }
}

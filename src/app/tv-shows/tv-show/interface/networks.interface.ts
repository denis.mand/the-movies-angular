export interface INetworks {
  name: string;
  id: number;
  logo_path: string;
  origin_country: string;
}

import { ICreator } from './creator.interface';
import { ILastEpisodeToAir } from './last-episode-to-air.interface';
import { INetworks } from './networks.interface';
import { IProductionCompanies } from '../../../movies/interfaces/production-companies.interface';
import { ISeason } from './season.interface';
import { IGenre } from '../../../movies/interfaces/genres.interface';

export interface ITvShowRequest {
  backdrop_path: string | null;
  created_by: ICreator[];
  episode_run_time: number[];
  first_air_date: string;
  genres_ids?: number[];
  genres?: IGenre[];
  homepage: string;
  id: number;
  in_production: boolean;
  language: string[];
  last_air_date: string;
  last_episode_to_air: ILastEpisodeToAir;
  name: string;
  next_episode_to_air: null;
  networks: INetworks[];
  number_of_episodes: number;
  number_of_seasons: number;
  origin_country: string[];
  original_language: string;
  original_name: string;
  overview: string;
  popularity: number;
  poster_path: string | null;
  production_companies: IProductionCompanies[];
  seasons: ISeason[];
  status: string;
  type: string;
  vote_average: number;
  vote_count: number;
}

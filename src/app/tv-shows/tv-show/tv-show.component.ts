import { Component, OnInit } from '@angular/core';
import { TvShowRepository } from '../model/tv-show.repository';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { TvShowModel } from '../model/tv-show.model';

@Component({
  templateUrl: './tv-show.component.html',
})
export class TvShowComponent implements OnInit {
  show: TvShowModel = new TvShowModel();
  page: number;

  constructor(
    private repository: TvShowRepository,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this.getTvShow(parseInt(this.route.snapshot.paramMap.get('id'), 10));
  }
  getTvShow(id: number): void {
    this.repository.getTvShow(id).subscribe(
      tvShow => (this.show = tvShow),
      () => {
        this.toastr.success('This id is undefined');
        this.router.navigateByUrl('/tv-shows');
      }
    );
  }
}

import { Component, OnInit } from '@angular/core';
import { TvShowModel } from './model/tv-show.model';
import { TvShowRepository } from './model/tv-show.repository';
import { IGenre } from '../movies/interfaces/genres.interface';
import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: 'tv-shows.component.html',
})
export class TvShowsComponent implements OnInit {
  shows: TvShowModel[];
  genre: IGenre;
  page: number;
  totalResults: number;

  constructor(private repository: TvShowRepository, private route: ActivatedRoute) {
    this.genre = {
      id: this.route.snapshot.params.id,
      name: this.route.snapshot.queryParams.genre,
    };
  }

  ngOnInit() {
    this.loadTvShows();
  }
  loadTvShows(page: number = 1) {
    this.genre && this.genre.id
      ? this.getTvShows(page, this.genre.id.toString())
      : this.getTvShows(page);
  }

  getTvShows(page: number, genre?: string): void {
    this.repository.getTvShows(page, genre).subscribe(tvShow => {
      this.shows = tvShow.results;
      this.totalResults = tvShow.total_results;
    });
  }
  pageChange(page: number) {
    this.page = page;
    this.loadTvShows(page);
  }
}

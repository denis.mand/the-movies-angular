import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from '../app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { ClickOutsideModule } from 'ng-click-outside';
import { ToggleNextElementDirective } from '../../directives/toggle-next-element.directive';
import { CollapseNavMenuDirective } from '../../directives/collapse-nav-menu.directive';
import { FooterComponent } from './footer/footer.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SidebarRowComponent } from './sidebar/sidebar-row.component';

@NgModule({
  imports: [AppRoutingModule, BrowserModule, ClickOutsideModule],
  declarations: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    SidebarRowComponent,
    ToggleNextElementDirective,
    CollapseNavMenuDirective,
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    SidebarRowComponent,
    ToggleNextElementDirective,
    CollapseNavMenuDirective,
  ],
})
export class LayoutModule {}

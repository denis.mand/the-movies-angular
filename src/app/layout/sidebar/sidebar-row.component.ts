import { Component, Input } from '@angular/core';
import { MovieModel } from '../../movies/model/movie.model';

@Component({
  selector: 'app-sidebar-row',
  templateUrl: 'sidebar-row.component.html',
  styleUrls: ['sidebar.component.scss'],
})
export class SidebarRowComponent {
  private _label: string;
  private _value: string | number;

  @Input()
  set label(label: string) {
    this._label = label;
  }

  get label(): string {
    return this._label;
  }

  @Input()
  set value(value: string | number) {
    this._value = value;
  }

  get value(): string | number {
    return this._value;
  }
}

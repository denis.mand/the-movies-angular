import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: 'sidebar.component.html',
  styleUrls: ['sidebar.component.scss'],
})
export class SidebarComponent {
  private _title: string;

  @Input()
  set title(title: string) {
    this._title = title;
  }

  get title(): string {
    return this._title;
  }
}

import { Component } from '@angular/core';
import { AuthenticationService } from '../../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss'],
})
export class HeaderComponent {
  ratingShow: boolean = false;

  constructor(public auth: AuthenticationService) {}

  toggleDropdown(dropdown: string): void {
    this[dropdown] = !this[dropdown];
  }

  hideDropdown(dropdown: string): void {
    this[dropdown] = false;
  }
}

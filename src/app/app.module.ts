import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { FormsModule } from '@angular/forms';
import { LayoutModule } from './layout/layout.module';
import { MoviesModule } from './movies/movies.module';
import { PeopleModule } from './people/people.module';
import { RatingModule } from './rating/rating.module';
import { TvShowsModule } from './tv-shows/tv-shows.module';
import { HomeModule } from './home/home.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AuthModule,
    LayoutModule,
    MoviesModule,
    PeopleModule,
    RatingModule,
    TvShowsModule,
    HomeModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}

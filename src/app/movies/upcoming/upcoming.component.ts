import { Component, OnInit } from '@angular/core';
import { MovieModel } from '../model/movie.model';
import { MovieRepository } from '../model/movie.repository';

@Component({
  templateUrl: './upcoming.components.html',
})
export class UpcomingComponent implements OnInit {
  movies: MovieModel[];
  totalResults: number;
  page: number;

  constructor(private repository: MovieRepository) {}
  ngOnInit(): void {
    this.getUpcomingMovies(1);
  }

  getUpcomingMovies(page: number = 1): void {
    this.repository.getUpcomingMovie(page).subscribe(item => {
      this.movies = item.results;
      this.totalResults = item.total_results;
    });
  }

  pageChange(page: number) {
    this.page = page;
    this.getUpcomingMovies(page);
  }
}

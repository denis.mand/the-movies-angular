import { ICast } from '../../people/interface/cast.interface';
import { ICrew } from '../../people/interface/crew.interface';

export interface IPeople {
  cast: ICast[];
  crew: ICrew[];
  id: number;
}

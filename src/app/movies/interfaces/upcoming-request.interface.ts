import { MovieModel } from '../model/movie.model';
import { IDates } from './dates.interface';
import { IPaginationRequest } from '../../interface/pagination-request.interface';

export interface IUpcomingRequest extends IPaginationRequest {
  results: MovieModel[];
  dates: IDates;
}

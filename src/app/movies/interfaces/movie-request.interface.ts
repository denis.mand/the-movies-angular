import { MovieModel } from '../model/movie.model';
import { IPaginationRequest } from '../../interface/pagination-request.interface';

export interface IMovieRequest extends IPaginationRequest {
  results: MovieModel[];
}

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequestServices } from '../../../services/http-request.services';
import { MovieModel } from './movie.model';
import { IPeople } from '../interfaces/people.interface';
import { IMovieRequest } from '../interfaces/movie-request.interface';
import { IUpcomingRequest } from '../interfaces/upcoming-request.interface';

@Injectable()
export class MovieDataSource {
  constructor(private http: HttpRequestServices) {}

  getMovies(page?: number, genre?: string): Observable<IMovieRequest> {
    return this.http.get('discover/movie', { page, with_genres: genre });
  }

  getMovie(id: string): Observable<MovieModel> {
    return this.http.get(`movie/${id}`);
  }

  getUpcomingMovies(page?: number): Observable<IUpcomingRequest> {
    return this.http.get('movie/upcoming', { page });
  }

  getTeam(id: string): Observable<IPeople> {
    return this.http.get(`movie/${id}/credits`);
  }
}

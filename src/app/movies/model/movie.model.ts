import { Injectable } from '@angular/core';
import { IGenre } from '../interfaces/genres.interface';
import { IProductionCompanies } from '../interfaces/production-companies.interface';
import { ICountry } from '../interfaces/country.interface';

@Injectable({
  providedIn: 'root',
})
export class MovieModel {
  public id: number;
  public popularity: number;
  public video: string | boolean;
  public adult: boolean;
  public backdrop_path: string | null | boolean;
  public poster_path: string | null | boolean;
  public original_language: string;
  public title: string;
  public vote_count: number;
  public vote_average: number;
  public overview: string;
  public release_date: string;
  public belongs_to_collection?: any;
  public budget?: number;
  public genre?: number[];
  public genres?: IGenre[];
  public homepage?: string;
  public imdb_id?: string;
  public original_title?: string;
  public production_companies?: IProductionCompanies;
  public production_countries?: ICountry[];
  public revenue?: number;
  public runtime?: number;
  public status?: string;
  public tagline?: string;
}

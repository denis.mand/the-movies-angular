import { Injectable } from '@angular/core';
import { MovieDataSource } from './movie.datasource';
import { MovieModel } from './movie.model';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IPeople } from '../interfaces/people.interface';
import { IMovieRequest } from '../interfaces/movie-request.interface';
import { IUpcomingRequest } from '../interfaces/upcoming-request.interface';

@Injectable()
export class MovieRepository {
  imageRoute: string = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2';

  constructor(private dataSource: MovieDataSource) {}

  getMovies(page?: number, genre?: string): Observable<IMovieRequest> {
    return this.dataSource.getMovies(page, genre).pipe(
      map(item => {
        item.results.map(movie => {
          if (movie.poster_path) {
            movie.poster_path = this.imageRoute + movie.poster_path;
          }
        });
        return item;
      })
    );
  }

  getMovie(id: string): Observable<MovieModel> {
    return this.dataSource.getMovie(id).pipe(
      map(movie => {
        if (movie.poster_path) {
          movie.poster_path = this.imageRoute + movie.poster_path;
        }
        return movie;
      })
    );
  }

  getUpcomingMovie(page?: number): Observable<IUpcomingRequest> {
    return this.dataSource.getUpcomingMovies(page).pipe(
      map(movies => {
        movies.results.map(movie => {
          if (movie.poster_path) {
            movie.poster_path = this.imageRoute + movie.poster_path;
          }
        });
        return movies;
      })
    );
  }

  getTeam(id: string): Observable<IPeople> {
    return this.dataSource.getTeam(id).pipe(
      map(people => {
        people.cast.map(actor => {
          if (actor.profile_path) {
            actor.profile_path = this.imageRoute + actor.profile_path;
          }
        });
        people.crew.map(actor => {
          if (actor.profile_path) {
            actor.profile_path = this.imageRoute + actor.profile_path;
          }
        });
        return people;
      })
    );
  }
}

import { Component, Input } from '@angular/core';
import { MovieModel } from '../model/movie.model';

@Component({
  selector: 'movie-card',
  templateUrl: './movie-card.component.html',
})
export class MovieCardComponent {
  private _movie: MovieModel;

  @Input()
  set movie(movie: MovieModel) {
    this._movie = movie;
  }

  get movie(): MovieModel {
    return this._movie;
  }
}

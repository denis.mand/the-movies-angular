import { Component, OnInit } from '@angular/core';
import { MovieRepository } from './model/movie.repository';
import { MovieModel } from './model/movie.model';
import { ActivatedRoute } from '@angular/router';
import { IGenre } from './interfaces/genres.interface';

@Component({
  templateUrl: 'movies.component.html',
})
export class MoviesComponent implements OnInit {
  movies: MovieModel[];
  totalResults: number;
  genre: IGenre;
  page: number = 1;

  constructor(private repository: MovieRepository, private route: ActivatedRoute) {
    this.genre = {
      id: this.route.snapshot.params.id,
      name: this.route.snapshot.queryParams.genre,
    };
  }

  ngOnInit() {
    this.loadMovies();
  }
  loadMovies(page: number = 1) {
    if (this.genre && this.genre.id) {
      this.getMovies(page, this.genre.id.toString());
    } else {
      this.getMovies(page);
    }
  }
  getMovies(page?: number, genre?: string): void {
    this.repository.getMovies(page, genre).subscribe(item => {
      this.movies = item.results;
      this.totalResults = item.total_results;
    });
  }
  pageChange(page: number) {
    this.page = page;
    this.loadMovies(page);
  }
}

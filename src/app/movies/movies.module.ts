import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MovieRepository } from './model/movie.repository';
import { MovieDataSource } from './model/movie.datasource';
import { MoviesComponent } from './movies.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpRequestServices } from '../../services/http-request.services';
import { MovieCardComponent } from './card/movie-card';
import { MovieComponent } from './movie/movie.component';
import { AppRoutingModule } from '../app-routing.module';
import { LayoutModule } from '../layout/layout.module';
import { HoursPipe } from '../../pipes/hours.pipe';
import { PeopleModule } from '../people/people.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { UpcomingComponent } from './upcoming/upcoming.component';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    LayoutModule,
    PeopleModule,
    NgxPaginationModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
  ],
  providers: [MovieRepository, MovieDataSource, HttpRequestServices],
  declarations: [MoviesComponent, MovieCardComponent, MovieComponent, HoursPipe, UpcomingComponent],
  exports: [MoviesComponent, MovieCardComponent, MovieComponent, HoursPipe, UpcomingComponent],
})
export class MoviesModule {}

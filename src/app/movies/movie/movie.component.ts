import { Component, OnInit } from '@angular/core';
import { MovieModel } from '../model/movie.model';
import { MovieRepository } from '../model/movie.repository';
import { ActivatedRoute, Router } from '@angular/router';
import { ICast } from '../../people/interface/cast.interface';
import { ICrew } from '../../people/interface/crew.interface';
import { ToastrService } from 'ngx-toastr';

@Component({
  templateUrl: 'movie.component.html',
  styleUrls: ['movie.component.scss'],
})
export class MovieComponent implements OnInit {
  movie: MovieModel = new MovieModel();
  cast: ICast[];
  creator: ICrew[];

  constructor(
    private repository: MovieRepository,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService
  ) {}
  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.getMovie(id);
    this.getTeam(id);
  }
  getMovie(id) {
    this.repository.getMovie(id).subscribe(
      item => (this.movie = item),
      () => {
        this.toastr.success('This id is undefined');
        this.router.navigateByUrl('/movies');
      }
    );
  }
  getTeam(id) {
    this.repository.getTeam(id).subscribe(people => {
      this.cast = people.cast;
      this.creator = people.crew;
    });
  }
}

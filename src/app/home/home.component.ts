import { Component, OnInit } from '@angular/core';
import { MovieModel } from '../movies/model/movie.model';
import { HomeRepository } from './model/home.repository';
import { TvShowModel } from '../tv-shows/model/tv-show.model';

@Component({
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  nowPlayingMovies: MovieModel[];
  popularMovies: MovieModel[];
  airingTvShow: TvShowModel[];
  popularTvShows: TvShowModel[];

  constructor(private repository: HomeRepository) {}

  ngOnInit(): void {
    this.getNowPlayingMovies();
    this.getPopularMovies();
    this.getAiringTvShow();
    this.getPopularTvShow();
  }

  getNowPlayingMovies() {
    this.repository.getNowPlayingMovies().subscribe(item => {
      this.nowPlayingMovies = item.results.slice(0, 6);
    });
  }

  getPopularMovies() {
    this.repository.getPopularMovies().subscribe(item => {
      this.popularMovies = item.results.slice(0, 6);
    });
  }

  getAiringTvShow() {
    this.repository.getAiringTvShow().subscribe(item => {
      this.airingTvShow = item.results.slice(0, 6);
    });
  }

  getPopularTvShow() {
    this.repository.getPopularTvShow().subscribe(item => {
      this.popularTvShows = item.results.slice(0, 6);
    });
  }
}

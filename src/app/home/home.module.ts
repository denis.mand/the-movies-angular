import { NgModule } from '@angular/core';
import { HomeDataSource } from './model/home.datasource';
import { HomeRepository } from './model/home.repository';
import { HomeComponent } from './home.component';
import { CommonModule } from '@angular/common';
import { TvShowsModule } from '../tv-shows/tv-shows.module';
import { MoviesModule } from '../movies/movies.module';

@NgModule({
  imports: [CommonModule, TvShowsModule, MoviesModule],
  providers: [HomeDataSource, HomeRepository],
  declarations: [HomeComponent],
  exports: [HomeComponent],
})
export class HomeModule {}

import { Injectable } from '@angular/core';
import { HomeDataSource } from './home.datasource';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { IMovieRequest } from '../../movies/interfaces/movie-request.interface';
import { ITvShowsRequest } from '../../tv-shows/interface/tv-shows-request.interface';

@Injectable()
export class HomeRepository {
  imageRoute: string = 'https://image.tmdb.org/t/p/w185_and_h278_bestv2';

  constructor(private dataSource: HomeDataSource) {}

  getNowPlayingMovies(page?: number) {
    return this.dataSource.getNowPlayingMovies(page).pipe(
      map(item => {
        item.results.map(movie => {
          if (movie.poster_path) {
            movie.poster_path = this.imageRoute + movie.poster_path;
          }
        });
        return item;
      })
    );
  }
  getPopularMovies(page?: number): Observable<IMovieRequest> {
    return this.dataSource.getPopularMovies(page).pipe(
      map(item => {
        item.results.map(movie => {
          if (movie.poster_path) {
            movie.poster_path = this.imageRoute + movie.poster_path;
          }
        });
        return item;
      })
    );
  }
  getAiringTvShow(page?: number): Observable<ITvShowsRequest> {
    return this.dataSource.getAiringTvShow(page).pipe(
      map(item => {
        item.results.map(show => {
          if (show.poster_path) {
            show.poster_path = this.imageRoute + show.poster_path;
          }
        });
        return item;
      })
    );
  }
  getPopularTvShow(page?: number): Observable<ITvShowsRequest> {
    return this.dataSource.getPopularTvShow(page).pipe(
      map(item => {
        item.results.map(show => {
          if (show.poster_path) {
            show.poster_path = this.imageRoute + show.poster_path;
          }
        });
        return item;
      })
    );
  }
}

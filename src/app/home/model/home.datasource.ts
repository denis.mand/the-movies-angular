import { Injectable } from '@angular/core';
import { HttpRequestServices } from '../../../services/http-request.services';
import { INowPlayingRequest } from '../interface/now-playing-request.interface';
import { Observable } from 'rxjs';
import { IMovieRequest } from '../../movies/interfaces/movie-request.interface';
import { ITvShowsRequest } from '../../tv-shows/interface/tv-shows-request.interface';

@Injectable()
export class HomeDataSource {
  constructor(private http: HttpRequestServices) {}

  getNowPlayingMovies(page?: number): Observable<INowPlayingRequest> {
    return this.http.get('movie/now_playing', { page });
  }

  getPopularMovies(page?: number): Observable<IMovieRequest> {
    return this.http.get('movie/popular', { page });
  }

  getAiringTvShow(page?: number): Observable<ITvShowsRequest> {
    return this.http.get('tv/airing_today', { page });
  }

  getPopularTvShow(page?: number): Observable<ITvShowsRequest> {
    return this.http.get('tv/popular', { page });
  }
}

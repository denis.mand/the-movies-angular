import { IPaginationRequest } from '../../interface/pagination-request.interface';
import { IDates } from '../../movies/interfaces/dates.interface';
import { MovieModel } from '../../movies/model/movie.model';

export interface INowPlayingRequest extends IPaginationRequest {
  dates: IDates;
  results: MovieModel[];
}
